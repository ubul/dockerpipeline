import os
from flask import Flask

context = os.getenv('CONTEXT', 'w1')

app = Flask(__name__)


@app.route('/')
def index():
    name = "This is the route."
    return name


@app.route('/' + context)
def hello():
    name = "Hello world!"
    return name


if __name__ == "__main__":
    print 'Context is {}'.format(context)
    app.run('0.0.0.0', 5000)
