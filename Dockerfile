FROM python:2.7-alpine

ADD main.py /

RUN pip install flask

CMD ["python", "./main.py"]